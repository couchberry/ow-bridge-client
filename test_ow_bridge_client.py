#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import zmq
import pytest
import threading
from ow_bridge_client import Client, Sensor
from ow_bridge_client import ClientTimeout, ClientError


def test_init():
    root = Sensor()
    assert root.path == '/'
    assert root.client.zmq_address == Client.DEFAULT_ZMQ_ADDRESS

    zmq_address = "tcp://127.0.0.1:1234"
    root = Sensor(client=Client(zmq_address))
    assert root.path == '/'
    assert root.client.zmq_address == zmq_address


def test_connection_timeout():
    zmq_address = "tcp://127.0.0.1:9999"
    client = Client(zmq_address, recv_timeout=0, linger_timeout=0)
    with pytest.raises(ClientTimeout):
        client.execute({})


def test_connection_reset():
    zmq_address = "tcp://127.0.0.1:9999"
    client = Client(zmq_address, recv_timeout=0, linger_timeout=0)
    zmq_socket = client.zmq_socket
    assert zmq_socket is client.zmq_socket
    with pytest.raises(ClientTimeout):
        client.execute({})
        assert zmq_socket is not client.zmq_socket


def test_error_response():

    def reply_with_error():
        zmq_address = Client.DEFAULT_ZMQ_ADDRESS
        ctx = zmq.Context()
        rep = ctx.socket(zmq.REP)
        rep.bind(zmq_address)
        rep.recv_json()
        rep.send_json({"error": "S**t happens"})
        rep.close()

    threading.Thread(target=reply_with_error).start()
    root = Sensor()
    with pytest.raises(ClientError):
        root.sensors


def test_list_sensors():

    def reply_list_of_paths():
        zmq_address = Client.DEFAULT_ZMQ_ADDRESS
        ctx = zmq.Context()
        rep = ctx.socket(zmq.REP)
        rep.bind(zmq_address)
        rep.recv_json()
        rep.send_json(["a", "b"])
        rep.close()

    threading.Thread(target=reply_list_of_paths).start()
    root = Sensor()
    sensors = root.sensors
    assert [s for s in sensors if isinstance(s, Sensor)]
    assert sensors[0].path == "a"
    assert sensors[1].path == "b"


def test_list_entries():

    entries = ["a", "b"]

    def reply_list_of_paths():
        zmq_address = Client.DEFAULT_ZMQ_ADDRESS
        ctx = zmq.Context()
        rep = ctx.socket(zmq.REP)
        rep.bind(zmq_address)
        rep.recv_json()
        rep.send_json(entries)
        rep.close()

    threading.Thread(target=reply_list_of_paths).start()
    root = Sensor()
    assert root.entries == entries


def test_read_entry():

    value = "value"
    entry_name = "temperature"
    entries = [entry_name, "A", "B", "C"]

    def reply_with_value():
        zmq_address = Client.DEFAULT_ZMQ_ADDRESS
        ctx = zmq.Context()
        rep = ctx.socket(zmq.REP)
        rep.bind(zmq_address)
        rep.recv_json()
        rep.send_json(entries)
        msg = rep.recv_json()
        assert msg["entry_name"] == entry_name
        rep.send_json(value)
        rep.close()

    threading.Thread(target=reply_with_value).start()
    root = Sensor()
    assert root[entry_name] == value
    with pytest.raises(KeyError):
        root["nonexisting"]


def test_write_entry():

    value = "value"
    entry_name = "temperature"
    entries = [entry_name, "A", "B", "C"]

    def reply_with_value():
        zmq_address = Client.DEFAULT_ZMQ_ADDRESS
        ctx = zmq.Context()
        rep = ctx.socket(zmq.REP)
        rep.bind(zmq_address)
        rep.recv_json()
        rep.send_json(entries)
        msg = rep.recv_json()
        assert msg["entry_name"] == entry_name
        assert msg["value"] == value
        rep.send_json(value)
        rep.close()

    threading.Thread(target=reply_with_value).start()
    root = Sensor()
    root[entry_name] = value
    with pytest.raises(KeyError):
        root["nonexisting"] = value


def test_repr():
    c = Client()
    s = Sensor()
    assert repr(c) in repr(s)
