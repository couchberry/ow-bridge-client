#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8


"""
================
ow_bridge_client
================

Client library for https://gitlab.com/couchberry/ow-bridge

It uses zmq to communicate with `ow_bridge`.


Simple usage
============

from ow_bridge_client import Sensor

root = Sensor()

"""

import zmq
import logging
import collections

LOGGER = logging.getLogger('ow_bridge_client')

try:
    # py2
    str_type = basestring
except NameError:
    # py3
    str_type = str


class ClientError(Exception):
    pass


class ClientTimeout(ClientError):
    pass


class Client:
    """
    Represents connection to the `ow_bridge`.
    """

    DEFAULT_ZMQ_ADDRESS = "tcp://127.0.0.1:4305"

    def __init__(
        self,
        zmq_address=None,
        recv_timeout=5000,
        linger_timeout=5000,
    ):
        self.recv_timeout = recv_timeout
        self.linger_timeout = linger_timeout
        self.zmq_address = zmq_address or self.DEFAULT_ZMQ_ADDRESS
        self.zmq_socket = self._create_zmq_socket()

    def _reset_connection(self):
        LOGGER.debug("Reseting connection.")
        self.zmq_socket.close()
        self.zmq_socket = self._create_zmq_socket()

    def _create_zmq_socket(self):
        ctx = zmq.Context()
        socket = ctx.socket(zmq.REQ)
        socket.setsockopt(zmq.LINGER, self.linger_timeout)
        socket.connect(self.zmq_address)
        return socket

    def execute(self, msg):
        LOGGER.debug("Sending: %s", msg)
        self.zmq_socket.send_json(msg)
        if self.zmq_socket.poll(self.recv_timeout):
            response = self.zmq_socket.recv_json()
            LOGGER.debug("Received: %s", response)
            if isinstance(response, collections.Mapping):
                if "error" in response:
                    raise ClientError(response["error"])
            return response
        # socket is now in bad state we need to reset connection
        self._reset_connection()
        raise ClientTimeout("Timeout passed while waiting for response.")

    def __repr__(self):
        return "%s(zmq_address=%r, recv_timeout=%r, linger_timeout=%r)" % (
            self.__class__.__name__,
            self.zmq_address,
            self.recv_timeout,
            self.linger_timeout,
        )


class Sensor:
    """
    Represents sensor/directory as seen by `owserver`.
    """

    def __init__(self, path="/", client=None, use_cache=True):
        if client is None:
            client = Client()
        self.client = client
        self.path = path
        self.use_cache = use_cache
        self._entries = None

    def _execute(self, data):
        msg = {"use_cache": self.use_cache, "path": self.path}
        msg.update(data)
        return self.client.execute(msg)

    @property
    def sensors(self):
        resp = self.client.execute({"action": "list_sensors"})
        return [
            Sensor(client=self.client, path=path, use_cache=self.use_cache)
            for path in resp
        ]

    def _init_entries(self):
        self._entries = self._execute({"action": "list_entries"})

    @property
    def entries(self):
        if self._entries is None:
            self._init_entries()
        return self._entries

    def _read_entry(self, entry_name):
        return self._execute({
            "action": "read_entry",
            "entry_name": entry_name,
        })

    def _write_entry(self, entry_name, value):
        return self._execute({
            "action": "write_entry",
            "entry_name": entry_name,
            "value": value,
        })

    def __getitem__(self, key):
        if key in self.entries:
            return self._read_entry(key)
        raise KeyError(key)

    def __setitem__(self, key, value):
        if key in self.entries:
            return self._write_entry(key, value)
        raise KeyError(key)

    def __repr__(self):
        return "%s(path=%r, client=%r, use_cache=%r)" % (
            self.__class__.__name__,
            self.path,
            self.client,
            self.use_cache,
        )
